<%-- 
    Document   : comprador
    Created on : 11-nov-2018, 19:18:10
    Author     : JuanEsteban
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.List"%>
<%@page import="Clases.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/styles.css"/>
        <title>Comprador</title>
    </head>
    <body class="container">
        <h1>Lista de cosas que puede comprar</h1>
        <%
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
            List<Producto> ps = Producto.consultarProductos();
            if(ps != null){
                for (Producto p : ps){
                    out.println("<div id='"+ p.getId() +"'class='item'>");
                    out.println("<h1>" + p.getNombre() +  "</h1>");
                    out.println("<p>" + p.getDescripcion() +  "</p>");
                    out.println("<img src='" + p.getFoto() +  "'>");
                    String currency = format.format(p.getPrecio());
                    out.println("<h3>$<span class='precio'>" + p.getPrecio() +  "</span></h3>");
                    out.println("<button class='buy btn btn-info'>Comprar</button>");
                    out.println("</div>");
                }
            }else{
                out.println(Producto.Error);
            }
        %>
        
        <form action="RegistrarPagos" id="registro">
            <input type="text" name="elements" class="hide" id="elements">
            <h2>Información personal</h2>
            <label>Cédula<input type="text" name="cedula"></label>
            <label>Nombre<input type="text" name="nombre"></label>
            <p>total a pagar: $<input id="total" name="total" type="text"></p>
            <input type="submit" value="Pagar" class="btn btn-success btn-lg">
        </form>
        
        <script type="text/javascript">
            var elements = [];
            var precio = 0;
            CalculateTotal();
            $(document).ready(function(){
                $(".buy").click(function(e){
                    var parent = $(this).parent(); 
                    if(parent.hasClass("selected")){
                        var index = elements.indexOf(parent.attr('id'));
                        elements.splice(index, 1);
                        $(this).html("Comprar");
                        $(this).removeClass("btn-danger");
                        $(this).addClass("btn-info");
                        
                    }else{
                        elements.push(parent.attr('id'));
                        $(this).html("Eliminar");
                        $(this).removeClass("btn-info");
                        $(this).addClass("btn-danger");
                    }
                    parent.toggleClass("selected");
                    CalculateTotal();
                });
                
                
            });
            
            function CalculateTotal(){
                $("#elements").val(elements.join());
                console.log($("#elements").val());
                precio = 0;
                elements.forEach(function(e) {
                    var price = $("#"+e +" .precio").html();
                    precio += parseInt(price);
                  });
                $("#total").val(precio);
            }
        </script>
    </body>
</html>
