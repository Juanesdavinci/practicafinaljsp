<%-- 
    Document   : factura
    Created on : 12-nov-2018, 9:49:04
    Author     : JuanEsteban
--%>

<%@page import="java.util.List"%>
<%@page import="Clases.Producto"%>
<%@page import="Clases.Usuario"%>
<%@page import="Clases.Factura"%>
<%
    int id= 0;
    String Error = "";
    try{
        id = Integer.parseInt(request.getParameter("id"));
    }catch(Exception ex){
       Error = "No se puede leer la factura " + request.getParameter("id");
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/styles.css"/>
        <title>Factura #<% out.print(id); %></title>
    </head>
    <body class="container">
        <h1>Factura #<% out.print(id); %></h1>
        <%
            if(!Error.equals("")){
                out.print("<div class='alert alert-danger' >"+Error+"</div>");
                out.print("<a href='index.jsp' class='btn btn-info'>Volver</a>");
                return;
            }
        %>
        
        <%
            
            //Leer la factura
            Factura f = new Factura();
            f.getFacturaByID(id);
            
            //Leer el usuario
            Usuario u = new Usuario();
            u.getUserByID(f.getIdCliente());
            out.print("<h2>"+ u.getNombre()+"</h2>");
            
            //Leer los artículos
            List<Producto> ps = Producto.consultarProductosByFacturaId(id);
            %>
        
        <table class="table table-bordered table-hover">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Precio</th>
            </tr>
        <%
            if(ps != null){
                for (Producto p : ps){
                    out.println("<tr><td>"+ p.getId() +"</td>");
                    out.println("<td>" + p.getNombre() +  "</td>");
                    out.println("<td>" + p.getDescripcion() +  "</td>");
                    out.println("<td>" + p.getPrecio()+  "</td>");
                    out.println("</tr>");
                }
            }else{
                out.println(Producto.Error);
            }
        %>
         </table>
         <h3>Total: $<%out.print(f.getTotal());%></h3>
    </body>
</html>
