<%-- 
    Document   : compra
    Created on : 11-nov-2018, 22:20:53
    Author     : JuanEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/styles.css"/>
        <title>Compra</title>
    </head>
    <body class="container">
         <h1>Gracias por su compra</h1>
        <p>Total: $<%
            out.print(request.getAttribute("Total"));
        %></p>
        <p>Factura #<%
            out.print(request.getAttribute("factura"));
        %></p>
        
        <a href="index.jsp" class="btn btn-info">Volver</a>
    </body>
</html>
