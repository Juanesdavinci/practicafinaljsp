<%-- 
    Document   : index
    Created on : 11-nov-2018, 18:56:51
    Author     : JuanEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <title>Inicio</title>
    </head>
    <body class="container">
        <h1>Escoge el tipo de usuario que eres</h1>
        <a href="comprador.jsp" class="btn btn-info">Comprador</a>
        <a href="Admin.jsp" class="btn btn-info">Administrador</a>
    </body>
</html>
