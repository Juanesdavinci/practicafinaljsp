<%-- 
    Document   : Admin
    Created on : 12-nov-2018, 9:24:43
    Author     : JuanEsteban
--%>

<%@page import="java.util.List"%>
<%@page import="Clases.Factura"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/styles.css"/>
        <title>Página de Administración</title>
    </head>
    <body class="container">
        <h1>Compras Realizadas</h1>
        <table class="table table-bordered table-hover">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Cliente</th>
                <th scope="col">Total</th>
                <th scope="col">Fecha</th>
                <th scope="col">Detalles</th>
            </tr>
        <%
            List<Factura> fs = Factura.ConsultarFacturas();
            if(fs != null){
                for (Factura p : fs){
                    out.println("<tr><td>"+ p.getId() +"</td>");
                    out.println("<td>" + p.getIdCliente() +  "</td>");
                    out.println("<td>" + p.getTotal() +  "</td>");
                    out.println("<td>" + p.getFecha()+  "</td>");
                    out.println("<td> <a href='factura.jsp?id="+p.getId()+"' class='btn btn-info'>Detalles</a></td>");
                    out.println("</tr>");
                }
            }else{
                out.println(Factura.Error);
            }
        
        %>
        </table>
        
        <a href="index.jsp" class="btn btn-info">Volver</a>
    </body>
</html>
