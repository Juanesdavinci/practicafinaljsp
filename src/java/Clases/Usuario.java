/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Conexion.Conn;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author JuanEsteban
 */
public class Usuario {
    private int Cedula;
    private String Nombre;

    public static String Error;
    /**
     * @return the Cedula
     */
    public int getCedula() {
        return Cedula;
    }

    /**
     * @return the Nobre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Cedula the Cedula to set
     */
    public void setCedula(int Cedula) {
        this.Cedula = Cedula;
    }

    /**
     * @param Nobre the Nobre to set
     */
    public void setNombre(String Nobre) {
        this.Nombre = Nobre;
    }
    
    public boolean RegistrarDB(){
        Conn conn = new Conn();
        String sql = "INSERT INTO `Usuario`(`Cedula`, `Nombre`) VALUES ('" +Cedula +"', '"+Nombre+"')";
        return conn.InsertarBD(sql);
    }
    
    public void getUserByID(int id){
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Usuario` WHERE `Cedula`= "+id;
        Error = sql;
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            while (rs.next()){
                this.Cedula = rs.getInt("Cedula");
                this.Nombre = rs.getString("Nombre");
            }
            conn.cerrarConexion();
        } catch (SQLException ex) {
            Error = ex.getMessage();
        }
    }
}
