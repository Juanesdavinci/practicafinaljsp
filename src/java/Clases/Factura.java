/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Conexion.Conn;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JuanEsteban
 */
public class Factura {
    private int id;
    private int idCliente;
    private int Total;
    private Date Fecha;
    
    public Factura(){}
    
    public Factura(int id, int idCliente, int Total, Date fecha) {
        this.id = id;
        this.idCliente = idCliente;
        this.Total = Total;
        this.Fecha = fecha;
    }
    
    public static String Error;
    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(int Total) {
        this.Total = Total;
    }
    
    public int getId(){
        return id;
    }
    
    public boolean RegistrarDB(){
        Conn conn = new Conn();
        String sql = "INSERT INTO `Factura`(`idCliente`, `Total`, `Fecha`) "
                + "VALUES ('" +getIdCliente() +"', '"+getTotal()+"', '"+ LocalDateTime.now() +"')";
        boolean b = conn.InsertarBD(sql);
        getInsertedID();
        return b ;
    }
    
    private void getInsertedID(){
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Factura` ORDER BY id DESC Limit 1";
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            while (rs.next()){
                id = rs.getInt("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean RegistrarProductos(String el[]){
        getInsertedID();
        Conn conn = new Conn();
         
        for (int i = 0; i < el.length; i++) {
            String sql = "INSERT INTO `Producto_Factura`(`idProducto`, `idFactura`) "
                    + "VALUES ("+el[i] +","+ id +")";
            conn.InsertarBD(sql);
        }
        return true;
    }
    
    public static List<Factura> ConsultarFacturas(){
        List<Factura> fs = new ArrayList();
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Factura`";
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            while (rs.next()){
                Factura f = new Factura(rs.getInt("id"),rs.getInt("idCliente") ,
                        rs.getInt("Total"), rs.getDate("Fecha"));
                fs.add(f);
            }
            conn.cerrarConexion();
            return fs;
        } catch (SQLException ex) {
            Error = ex.getMessage();
        }
        return null;
    }

    /**
     * @return the idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * @return the Total
     */
    public int getTotal() {
        return Total;
    }

    /**
     * @return the Fecha
     */
    public Date getFecha() {
        return Fecha;
    }
    
    public void getFacturaByID(int id){
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Factura` WHERE `id`= "+id;
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            while (rs.next()){
                this.id = rs.getInt("id");
                this.idCliente = rs.getInt("idCliente");
                this.Total = rs.getInt("Total");
                this.Fecha = rs.getDate("Fecha");
            }
            conn.cerrarConexion();
        } catch (SQLException ex) {
            Error = ex.getMessage();
        }
    }
}
