/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Conexion.Conn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JuanEsteban
 */
public class Producto {
    private int id;
    private int Precio;
    private String Nombre;
    private String Descripcion;
    private String Foto;
    
    public static String Error;
    public Producto(){}
    
    public Producto(int id, int Precio, String Nombre, String Descripcion, String Foto) {
        this.id = id;
        this.Precio = Precio;
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
        this.Foto = Foto;
    }
    
    
    
    
    public static List<Producto> consultarProductos(){
       List<Producto> productos  = new ArrayList();
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Producto`";
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            while (rs.next()){
                Producto p = new Producto(rs.getInt("id"),rs.getInt("Precio") ,
                        rs.getString("Nombre"), rs.getString("Descripcion"), 
                        rs.getString("Foto") );
                productos.add(p);
            }
            conn.cerrarConexion();
            return productos;
        } catch (SQLException ex) {
            Error = ex.getMessage();
        }
        return null;
    
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the Precio
     */
    public int getPrecio() {
        return Precio;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @return the Descripcion
     */
    public String getDescripcion() {
        return Descripcion;
    }

    /**
     * @return the Foto
     */
    public String getFoto() {
        return Foto;
    }
    
    
    public static List<Producto> consultarProductosByFacturaId(int id){
        List<Producto> productos  = new ArrayList();
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Producto_Factura` where `idFactura`= " + id;
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            while (rs.next()){
                Producto p = consultarProductosById(rs.getInt("idProducto"));
                if(p != null)
                    productos.add(p);
            }
            conn.cerrarConexion();
            return productos;
        } catch (SQLException ex) {
            Error += "<br>"+ ex.getMessage();
        }
        return null;
    }
    
    private static Producto consultarProductosById(int id){
        Conn conn = new Conn();
        String sql = "SELECT * FROM `Producto` WHERE `id`= "+id;
        ResultSet rs = conn.ConsultarBD(sql);
        try {
            Producto p = new Producto();
            while (rs.next()){
                 p = new Producto(rs.getInt("id"),rs.getInt("Precio") ,
                            rs.getString("Nombre"), rs.getString("Descripcion"), 
                            rs.getString("Foto") );
            }
            conn.cerrarConexion();
            return p;
        } catch (SQLException ex) {
            Error +="<br>"+ ex.getMessage();
        }
        return null;
    }
}
