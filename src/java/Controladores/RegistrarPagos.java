/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Clases.Factura;
import Clases.Producto;
import Clases.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author JuanEsteban
 */
@WebServlet(name = "RegistrarPagos", urlPatterns = {"/RegistrarPagos"})
public class RegistrarPagos extends HttpServlet {

    String elements;
    int Total;
    String Error;
    Usuario u;
    Factura f;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
                out.print("Sisas");
            if(CheckData(request)){
                
    //            RegistrarCliente();
                RegistrarFactura();
                RegistrarProductosFactura();
                request.setAttribute("Total", Total);       
                request.setAttribute("factura", f.getId());       
                //request.setAttribute("apuesta", "" + apuesta.number);
                RequestDispatcher disp = request.getRequestDispatcher("/compra.jsp");
                disp.forward(request, response);    
            }else{
                out.print(Error);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
    private boolean CheckData (HttpServletRequest request){
        String nombre, cedula;
        try{
            String el = request.getParameter("elements");
            if(!"".equals(el)){
                    elements = el;
            }else{Error = "Por favor ingrese un elemento a comprar"; return false;}
        }catch(Exception e){Error = "Por favor ingrese un elemento a comprar"; return false;}
        
        try{
            String sTotal = request.getParameter("total");
            if(!"".equals(sTotal)){
                    Total = Integer.parseInt(sTotal);
            }else{Error = "Error al leer el total"; return false;}
        }catch(Exception e){Error = "Error al leer el total"; return false;}
        
        try{
            String nom = request.getParameter("nombre");
            if(!"".equals(nom)){
                    nombre = nom;
            }else{Error = "Por favor digite un nombre"; return false;}
        }catch(Exception e){Error = "Por favor digite un nombre"; return false;}
        try{
            String ced = request.getParameter("cedula");
            if(!"".equals(ced)){
                    cedula = ced;
                    int cedulaInt = Integer.parseInt(cedula);
                    if(cedulaInt > 0){
                        RegistrarCliente(cedulaInt, nombre);
                    }else{Error = "Por favor digite una cantida de días válida"; return false;}
            }else{Error = "Por favor digite un marca"; return false;}
        }catch(Exception e){Error = "Por favor digite una cédula"; return false;}
        return true;
    }
    
    private void RegistrarCliente(int cedula, String nombre ){
        Usuario usr = new Usuario();
        usr.setCedula(cedula);
        usr.setNombre(nombre);
        usr.RegistrarDB();
        u = usr;
    }
    
    private void RegistrarFactura(){
        f = new Factura();
        f.setIdCliente(u.getCedula());
        f.setTotal(Total);
        f.RegistrarDB();
    }
    
    private void RegistrarProductosFactura(){
        String el[] =  elements.split(",");
        f.RegistrarProductos(el);
    }
}
