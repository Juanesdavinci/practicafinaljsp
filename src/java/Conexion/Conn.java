package Conexion;

import java.sql.*;

public class Conn {
    
    //Variables de conexión
    private String bd;
    private String dsn;
    private String user;
    private String pass;
    private Connection cn;
    
    //Variables de consulta
    private Statement s;
    private ResultSet rs;
    
    //Variables de Error
    private String Error = "";
    
    public Conn(){
        
        bd="VentaProductos";
        //dsn="jdbc:sqlserver://SALAN502-5;databaseName=" + bd + ";user=sa;password=Je123456";
        user = "root";
        pass = "root";
        dsn="jdbc:mysql://localhost:8889/"+bd;
    }
    
    private void abrirConexion(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            cn= DriverManager.getConnection(dsn, user, pass);
        }catch(ClassNotFoundException | SQLException ex){
            setError("abrirConexion -- " + ex.toString());
        }
    }
    public ResultSet ConsultarBD(String sql){
        abrirConexion();
        try{
            s = cn.createStatement();
            rs = s.executeQuery (sql);
            ResultSet result = rs;
            return result;
        }catch(SQLException ex){
            setError("ConsultarBD -- " + ex.getMessage());
            return null;
        }finally{
            //cerrarConexion();
        }
    }
    public boolean InsertarBD(String sql){
        abrirConexion();
        try{
            int filaguardada;
            s = cn.createStatement();
           filaguardada = s.executeUpdate(sql);
           //filaguardada = cstmt.executeUpdate();
           if (filaguardada == 1){
               return true;
           }else{
               
               return false;
           }
        }catch(SQLException ex){
            setError("InsertarBD -- " + ex.getMessage());
            return false;
        }finally{
            cerrarConexion();
        }
    }
    
    public void cerrarConexion(){
        try{
           cn.close();
        }catch(SQLException ex){
            setError("cerrarConexion -- " + ex.getMessage());
        }
    }
    
    /*    public boolean TestConnection(){
    try{
    String sql = "SELECT * FROM Usuario";
    cerrarConexion();
    }catch(Exception ex){
    setError("TestConnection -- " + ex.getMessage());
    return false;
    }
    }*/

    /**
     * @return the Error
     */
    public String getError() {
        return Error;
    }

    /**
     * @param Error the Error to set
     */
    public void setError(String Error) {
        this.Error += "<br>" +Error;
    }
}
